# Minor hack to allow Userena to work with LDAP
from django_auth_ldap.backend import LDAPBackend as Orig
import logging

class workaround(Orig):
    def authenticate(self, username=None, identification=None, password=None):
        if identification and not username:
            username = identification
        
        return super(workaround, self).authenticate(username=username, password=password)
