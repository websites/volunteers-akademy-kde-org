import os
import sys

os.environ['DJANGO_SETTINGS_MODULE'] = "volunteer_mgmt.settings"
os.environ['PYTHON_EGG_CACHE'] = "/srv/www/volunteers.akademy.kde.org/virtualenv/tmp/egg_cache"
os.environ['HOME'] = "/srv/www/volunteers.akademy.kde.org/fosdem-volunteers/"

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
